import * as React from 'react';
import {  useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';

export default function Hero() {
  const theme = useTheme();
  return (
    <Box
      id="hero"
    >
      <Container
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          pt: { xs: 10, sm: 15 },
          pb: { xs: 8, sm: 12 },
        }}
      >
        <Stack spacing={2} useFlexGap sx={{ width: { xs: '100%', sm: '70%' } }}>
          <Typography
            variant="h1"
            sx={{
              display: 'flex',
              flexDirection: { xs: 'column', md: 'row' },
              alignSelf: 'center',
              textAlign: 'center',
              fontSize: 'clamp(3.5rem, 10vw, 4rem)',
            }}
          >
            Welcome to&nbsp;
            <Typography
              component="span"
              variant="h2"
              sx={{
                fontSize: 'clamp(3rem, 10vw, 4rem)',
                color: theme.palette.primary.godColor
              }}
            >
              TotalDB
            </Typography>
          </Typography>
          <Typography
            textAlign="center"
            color="text.other"
            sx={{ alignSelf: 'center', width: { sm: '100%', md: '80%' } }}
          >
            Find, compare and analize all your units from Total War Warhammer 3 in a single
            place! Create custom units, factions, heroes and lords and see their stats side by side
            with all base game units!
          </Typography>
         

        </Stack>
      </Container>
    </Box>
  );
}
