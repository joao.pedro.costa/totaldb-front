import { Typography, alpha } from '@mui/material';

export default function backgroundColor(theme) {
    if (theme === "order") return `linear-gradient(180deg, #735A21, ${alpha('#999999', 0.7)} 115%)`
    if (theme === "undivided") return `linear-gradient(180deg, #390008, ${alpha('#171314', 0.8)} 75%)`
    if (theme === "khorne") return `linear-gradient(180deg, #75192F, ${alpha('#FFF700', 0.7)} 115%)`
    if (theme === "nurgle") return `linear-gradient(180deg, #849B63, ${alpha('#2b2a24', 1)} 95%)`
    if (theme === "tzeench") return `linear-gradient(180deg, #9C004A, ${alpha('#00506F', 0.95)} 40%)`
    if (theme === "slaanesh") return `linear-gradient(180deg, #5A005A, ${alpha('#842994', 0.95)} 115%)`
    return ""
}