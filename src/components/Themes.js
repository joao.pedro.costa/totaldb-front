
import { createTheme } from "@mui/material"
export const nurgle_theme = createTheme({
    palette: {
      God: {
        light:  "#005221",
        main:  "#005221",
        dark:  "#005221",
        constastText:  "#005221",
      },
    }
  })
  
export const khorne_theme = createTheme({
    palette: {
      God: {
        light:  "#6A0002",
        main:  "#6A0002",
        dark:  "#6A0002",
        constastText:  "#6A0002",
      },
    }
  })
  
  
export const tzeench_theme = createTheme({
    palette: {
      God: {
        light:  "#00506F",
        main:  "#00506F",
        dark:  "#00506F",
        constastText:  "#00506F",
      },
    }
  })
  
export const slaanesh_theme = createTheme({
    palette: {
      God: {
        light:  "#451636",
        main:  "#451636",
        dark:  "#451636",
        constastText:  "#451636",
      },
    }
  })
  
export const undivided_theme = createTheme({
    palette: {
      God: {
        light:  "#393939",
        main:  "#393939",
        dark:  "#393939",
        constastText:  "#393939",
      },
    }
  })
  
  
export const order_theme = createTheme({
    palette: {
      God: {
        light:  "#393121",
        main:  "#393121",
        dark:  "#393121",
        constastText:  "#393121",
      },
    }
  })
  
