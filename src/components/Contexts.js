'use Client'

import * as React from 'react'

const usr = {usr:"",pass:"", logged: false}

export const NavigateContext = React.createContext('/')

export const AccountContext = React.createContext({user: usr, loginFunc: () => {}, logoutFunc: () => {}})

// export default AccountContext
