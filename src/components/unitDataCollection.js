'use Client'

import * as React from 'react'
import { useTheme } from "@mui/system"
import {Pagination, alpha } from '@mui/material';
import Box from '@mui/material/Box'
import { DataGrid , gridClasses} from '@mui/x-data-grid';
import { useCookies } from 'react-cookie';
import { mainUrl } from '../app/ClientComponent';

const columns = [
    {field: 'name', headerName: 'Name', width:255, headerClassName: 'grid-theme--header'},
    {field: 'category', headerName: 'Category', width: 125, headerClassName: 'grid-theme--header'},
    {field: 'type', headerName: 'Unit Type', width: 150, headerClassName: 'grid-theme--header'},
    {field: 'faction', headerName: 'Faction', width: 100, headerClassName: 'grid-theme--header'},
    {field: 'health', headerName: 'Health', width: 100, headerClassName: 'grid-theme--header'},
    {field: 'leadership', headerName: 'Leadership', width: 100, headerClassName: 'grid-theme--header'},
    {field: 'speed', headerName: 'Speed', width: 100, headerClassName: 'grid-theme--header'},
    {field: 'meleeAttack', headerName: 'Melee Attack', width: 100, headerClassName: 'grid-theme--header'},
    {field: 'meleeDefense', headerName: 'Melee Defense', width: 120, headerClassName: 'grid-theme--header'},
    {field: 'isLeg', headerName: 'Is Unique', width: 100, headerClassName: 'grid-theme--header', type: 'boolean'},
    

]

function formatCategory(category) {
    if (category === "meleeInfantry") return "Melee Infantry";
    if (category === "missileInfantry") return "Missile Infantry";
    else return category.charAt(0).toUpperCase() + category.slice(1)
}


function createData(unitData, index) {
    return {
      id: index,
      name: unitData.name,
      category: formatCategory(unitData.category),
      isLeg: unitData.isLeg,
      type: unitData.charType,
      faction: unitData.faction === undefined? "none" : unitData.faction,
      health: unitData.health,
      leadership: unitData.leadership,
      speed: unitData.speed,
      meleeAttack: unitData.meleeAttack,
      meleeDefense: unitData.meleeDefense,
      }
  }


export default function UnitDataCollection({race, state}) {
    const theme = useTheme();
    const [rows, setRows] = React.useState([])
    const requests = {
        lords: state.showLord,
        heroes: state.showHero,
        meleeInfantry: state.showInfantry,
        missileInfantry: state.showMissile,
        cavalry: state.showCavalry,
        artillery: state.showArtillery,
        monster: state.showMonster
    }

    const [cookies, setCookies, removeCookies, updateCookies] = useCookies(["all"],{path:"/"})
    
    const getUnits = () => {
        var new_units = [];
        setRows([])
        updateCookies();
        console.log(cookies)
        const user = cookies.user === "" || cookies.user === undefined? "none" : cookies.user
        for (let [unitType, check] of Object.entries(requests)) {
            if (check)
                fetch(`http://${mainUrl}/db/getAllByRace/${race}/${unitType}/${user}`, {
                    method: "GET",
                  })
                  .then(res => {
                    return res.json()
                  })
                  .then(data => {
                    for (let unit of data) {
                        new_units = new_units.concat(createData(unit, new_units.length))
                        setRows(new_units)
                        
                    }
                  })
                  .catch(e => {console.log(e)})
        }
    }

    React.useEffect( () => getUnits(), [
        state.showLord,
        state.showHero,
        state.showInfantry,
        state.showMissile,
        state.showCavalry,
        state.showArtillery,
        state.showMonster,
        cookies.logged
    ])

    return (
        <div style ={{height:'auto', width:'100%'}}>
            <Box 
                sx={{
                    '& .grid-theme--header': {
                        backgroundColor: alpha(theme.palette.primary.godColor, 0.55),
                        fontWeight: '600',
                        fontSize: 16
                    },
                    '& .grid-theme--even-row': {
                        backgroundColor: alpha(theme.palette.text.primary, 0.2),
                    },
                    '& .grid-theme--odd-row': {
                        backgroundColor: alpha(theme.palette.primary.godColor, 0.5)
                    }

                }}
            >
            <DataGrid
                rows={rows}
                columns={columns}
                autoHeight = {true}
                sx={{
                    fontSize: 16,
                }}
                initialState={{
                    pagination: {
                        paginationModel: {page: 0, pageSize: 30}
                    },
                }}
                pageSizeOptions={[5,10,20,30]}
                getRowClassName={(params) => params.indexRelativeToCurrentPage % 2 === 0? 
                    'grid-theme--even-row' :
                    'grid-theme--odd-row' 
                }
                />
                </Box>
        </div>
    )

    
}