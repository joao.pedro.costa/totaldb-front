import * as React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import { useTheme } from '@mui/material/styles';
import Link from 'next/link';
import { mainUrl } from '../app/ClientComponent';

const logoFolder = "/races/";



const logoStyle = {
  width: '100px',
  height: '80px', 
  margin: '0 32px',
};

export default function LogoCollection() {
  const theme = useTheme();
  const [logos, setLogos] = React.useState([]);
  const [races, setRaces] = React.useState([]);
  React.useEffect(() => getAllLogos(), [])

  const getAllLogos = () => {
    fetch(`http://${mainUrl}/db/getAllRaces`, {
      method: "GET",
    })
      .then(res => {
        return res.json()
      })
      .then(data => {
          var new_logos = [];
          var new_races = []
          for (let race of data) {
            new_logos = new_logos.concat(logoFolder.concat(race, ".png"))       ;
            new_races = new_races.concat(race);
          }
          setLogos(new_logos);
          setRaces(new_races);
          
      })
      .catch(e => {
      console.log(e);
    });

  }

  

  
  
  return (
    <Box id="logoCollection" sx={{ py: 4 }}>
      <Typography
        component="p"
        variant="h3"
        align="center"
        color="text.primary"
      >
        Available races 
      </Typography>
      <Grid container justifyContent="center" sx={{py: 4, mt: 0.5}}>
        {logos.map((logo, index) => (
          <Grid item key={index}>
            <Link href={`/factions/${races[index]}`} passHref style={{textDecoration: 'none'}}>
            <img
              src={logo}
              alt={`Nation number ${index + 1}`}
              style={logoStyle}
            />
            </Link>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
}
