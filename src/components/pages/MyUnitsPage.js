


'use client'

import * as React from 'react';
import PropTypes from 'prop-types';

import Toolbar from '@mui/material/Toolbar';
import Footer from '../Footer';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import { Checkbox, FormControl, Grid, FormLabel, Typography, alpha, FormGroup, FormControlLabel, Paper } from '@mui/material';
import MuiDrawer from '@mui/material/Drawer'
import { styled, ThemeProvider, useTheme } from '@mui/material/styles';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import AutoAwesomeRoundedIcon from '@mui/icons-material/AutoAwesomeRounded';
import AppAppBar from '../AppAppBar';
import backgroundColor from '../BackgroundColor';
import MyUnitsCollection from '../MyUnitsCollection'
const drawerWidth = 240
import { useCookies } from 'react-cookie';




export default function Page({faction}) {
  const theme = useTheme();
  const [state, setState] = React.useState({
    showLord: true,
    showHero: false,
    showInfantry: false,
    showMissile: false,
    showCavalry: false,
    showArtillery: false,
    showMonster: false
})

  
const handleChange = (event) => {
    console.log(`got event from ${event.target.name}`)
    setState({
        ...state,
        [event.target.name]: event.target.checked,
    })
};

const {showLord, showHero, showInfantry, showMissile, showCavalry,
    showArtillery, showMonster} = state;

  const [cookies, setCookies]  = useCookies(["all"], {path: "/"})
  const ToggleBackgroundColor = (newTheme) => {theme.setGodTheme(newTheme);}
  return (
     <ThemeProvider theme = {theme}>
      <CssBaseline />
      {/* <Box height={1440} zIndex={0}> */}
      <AppAppBar toggleColor={ToggleBackgroundColor} />
      
      <Box
      id="my-units-page"
      height={1440}
      sx={(theme) => ({
        width: '100%',
        backgroundImage: backgroundColor(theme.godTheme),
        backgroundSize: '100% 100%',
        backgroundRepeat: 'no-repeat',
      })}
    >
         <Container maxWidth={false}
          id = 'container-1'
        sx={{
          height: '100%',
          width: '100%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          lg: 2400,
          pt: { xs: 5, sm: 10},pb: { xs: 0, sm: 0 },
        }}
      >
        <Container maxWidth={false}
        sx={{
          height: '100%',
          width: '100%',
          flexDirection: 'column',
          alignItems: 'center',
          pt: { xs: 0, sm: 5},pb: { xs: 0, sm: 0},
        }}
      >
        <Grid container spacing={4} alignItems={'top'}>
            <Grid item xs = {'auto'} md={'10'} lg={'auto'} alignContent={'left'}>
          {/* <Drawer variant='permanent' > */}
          <Paper sx ={(theme) =>({
            backgroundColor: 'rgba(255,255,255,0.4)',
            backdropFilter: 'blur(24px)',
            drawerWidth: 240,
            width:350,
            border: '2px solid',
            borderColor: 'divider',
            borderRadius: '0px',
            alignSelf: 'left',
            sm: 240 })}>  
          
            
          <FormControl sx={{m:3}}
            component={'fieldset'}
            variant='standard'
            >
          <Typography theme={theme} variant='h5' color='primary.godColor'> Select unit categories to show</Typography>
          {/* // <FormLabel component="legend">Select unit types to show </FormLabel>         */}
          <FormGroup>
            <FormControlLabel
              control={
            <Checkbox checked = {showLord} onChange = {handleChange} name = "showLord"/>
              }
                label= "Lords"                
              />
            <FormControlLabel
              control={
            <Checkbox checked = {showHero} onChange = {handleChange} name = "showHero"/>
              }
                label= "Heroes"                
              />
            <FormControlLabel
              control={
            <Checkbox checked = {showInfantry} onChange = {handleChange} name = "showInfantry"/>
              }
                label= "Melee Infantry"                
              />
            <FormControlLabel
              control={
            <Checkbox checked = {showMissile} onChange = {handleChange} name = "showMissile"/>
              }
                label= "Missile Infantry"                
              />
            <FormControlLabel
              control={
            <Checkbox checked = {showCavalry} onChange = {handleChange} name = "showCavalry"/>
              }
                label= "Cavalry"                
              />
            <FormControlLabel
              control={
            <Checkbox checked = {showArtillery} onChange = {handleChange} name = "showArtillery"/>
              }
                label= "Artillery and War Machines"                
              />
              <FormControlLabel
              control={
            <Checkbox checked = {showMonster} onChange = {handleChange} name = "showMonster"/>
              }
                label= "Monter Units"                
              />
            

            </FormGroup>
        </FormControl>
        </Paper>
        {/* </Drawer> */}
            </Grid>

        <Grid item xs = {'auto'} md = {'auto'} lg = {8}>
        <Paper sx ={(theme) =>({
            backgroundColor: 'rgba(255,255,255,0.4)',
            backdropFilter: 'blur(24px)',
            border: '2px solid',
            borderColor: 'divider',
            borderRadius: '0px',
            alignSelf: 'center',
            sm: 240 })}>  

            <MyUnitsCollection race = {faction} state={state}/>
        </Paper>

        </Grid>

        </Grid>



      </Container>
        
        <Footer />
        </Container>
        
      {/* </Box> */}
      </Box>
    </ThemeProvider>
    )
}