'use client'

import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import { ThemeProvider, useTheme } from '@mui/material/styles';
import AppAppBar from '../AppAppBar';
import Hero from '../Hero';
import Footer from '../Footer'
import LogoCollection from '../LogoCollection';
import Container from '@mui/material/Container';
import backgroundColor from '../BackgroundColor';
import { AccountContext } from '../Contexts';
import { useCookies } from 'react-cookie';

export default function LandingPage() {
  const theme = useTheme();
  const ToggleBackgroundColor = (newTheme) => {
    theme.setGodTheme(newTheme);
    // if (newTheme === 'order') {theme.setGodTheme('order'); }
    // else if (newTheme === 'undivided') {theme.setGodTheme('undivided')}
    // else if (newTheme === 'khorne') {theme.setGodTheme('khorne'); }
    // else if (newTheme === 'nurgle') {theme.setGodTheme('nurgle')}
    // else if (newTheme === 'tzeench') {theme.setGodTheme('tzeench')}
    // else if (newTheme === 'slaanesh') {theme.setGodTheme('slaanesh')}
  }

  const [cookies, setCookies] = useCookies(["all"],{path:"/"})
  setCookies("lastVisited", "/" ,{path:"/"})
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Box height={1440}>
      <AppAppBar toggleColor={ToggleBackgroundColor} />
      <Box sx={() => ({
        width: '100%',
        height: '100%',
        backgroundImage:backgroundColor(theme.godTheme),
        backgroundSize: '100% 100%',
        backgroundRepeat: 'no-repeat',
      })}>
        <Container disableGutters>
        <Hero />
        <LogoCollection />
        <Footer/>
        </Container>
      </Box>
      </Box>
    </ThemeProvider>
  );
}
