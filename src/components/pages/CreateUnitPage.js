'use client'

import * as React from 'react';

import Footer from '../Footer';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControlLabel from '@mui/material/FormControlLabel';
import MenuItem from '@mui/material/MenuItem';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { ThemeProvider, useTheme } from '@mui/material/styles';
import AppAppBar from '../AppAppBar';
import backgroundColor from '../BackgroundColor';
import { mainUrl } from '../../app/ClientComponent';
import { nurgle_theme, khorne_theme, tzeench_theme, slaanesh_theme, order_theme, undivided_theme } from '../Themes'
import { useCookies } from 'react-cookie';
import Checkbox from '@mui/material/Checkbox';

export default function CreateUnitPage({faction}) {
  const theme = useTheme();
  const [cookies, setCookies]  = useCookies(["all"],{path:"/"})
  const [makingReq, setMakingReq] = React.useState(false)
  const [nameValue, setName] = React.useState('')
  const [catValue, setCat] = React.useState('Lord')
  const [raceValue, setRace] = React.useState('')
  const [factionValue, setFaction] = React.useState('')
  const [typeValue, setType] = React.useState('')
  const [healthValue, setHelth] = React.useState('')
  const [leadershipValue, setLeadership] = React.useState('')
  const [speedValue, setSpeed] = React.useState('')
  const [mAttackValue, setMAttack] = React.useState('')
  const [mDefenseValue, setMDefense] = React.useState('')
  const [isLeg, setLeg] = React.useState(false)
  const [gotErr, setGotErr] = React.useState(false)
  const [errMsg, setErr] = React.useState('')

  const handleLegCheckbox = (event) => {
        setLeg(event.target.checked)
  }

  const clearForm = () => {
    setName('')
    // setCat('Lord')
    setType('')
    setRace('')
    setFaction('')
    setHelth('')
    setLeadership('')
    setSpeed('')
    setMDefense('')
    setMAttack('')
    // setLeg(true)
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    setMakingReq(true)
    const data = new FormData(event.currentTarget);
    console.log(data)
    const unit = {
        name: data.get('name'),
        category: data.get('category'),
        charType: data.get('unitType'),
        health: data.get('health'),
        leadership: data.get('leadership'),
        speed: data.get('speed'),
        meleeAttack: data.get('meleeAttack'),
        meleeDefense: data.get('meleeDefense'),
        isLeg: isLeg,
        owner: cookies.user
    }
    if (unit.category ==='Hero') unit.raceList = [data.get('race')]
    else {
        unit.race = data.get('race')
    }
    if (unit.category === "Lord")  unit.faction = data.get('faction') === ""? "none" : data.get('faction')
    
    const requestOptions = {
        headers: {'Content-Type': 'application/json'},
        method: "post",
        body: JSON.stringify(unit)
    }
    const addType = unit.category === 'Lord' || unit.category === 'Hero'? unit.category : 'Unit'
    console.log(`sending to http://${mainUrl}/db/add${addType}`)
    fetch(`http://${mainUrl}/db/add${addType}`, requestOptions)
        .then(res => {
            if (res.status === 200) {
                clearForm();
                setGotErr(false)
                setErr('')
            }
            else {
                setGotErr(true)
                setErr(`request returned ${res.status}`)
                console.log(`request returned ${res.status}`)
            }

        })
        .catch(e => {
            console.log(e)
            setErr(e)
            setGotErr(true)
        })
    setMakingReq(false)

  }

  
    
  
  const ToggleBackgroundColor = (newTheme) => {theme.setGodTheme(newTheme);}
  return (
     <ThemeProvider theme = {theme}>
      <CssBaseline />
      {/* <Box height={1440} zIndex={0}> */}
      <AppAppBar toggleColor={ToggleBackgroundColor} />
      
      <Box
      id="create-unit-page"
      height={1440}
      sx={(theme) => ({
        width: '100%',
        backgroundImage: backgroundColor(theme.godTheme),
        backgroundSize: '100% 100%',
        backgroundRepeat: 'no-repeat',
      })}
    >
         <Container maxWidth={false}
          id = 'container-1'
        sx={{
          height: '100%',
          width: '100%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          lg: 1500,
          pt: { xs: 5, sm: 10},pb: { xs: 0, sm: 0 },
        }}
      >
        <Typography variant='h2' color={theme.palette.primary.godColor}
            sx={{pt:{xs:10, sm:5}}}
        > Create Custom Unit</Typography>

        <Box component={"form"} onSubmit={handleSubmit} noValidate sx={{maxWidth:650}}>
        <TextField
              margin="normal"
              onChange= {(e) => setName(e.target.value)}
              required
              fullWidth
              id="name"
              label="Unit Name"
              name="name"
              autoComplete="name"
              value={nameValue}
              autoFocus
              variant='outlined'
            />
        <TextField
              margin="normal"
              onChange= {(e) => setCat(e.target.value)}
              required
              fullWidth
              id="category"
              label="Unit Category"
              name="category"
              defaultValue={"Lord"}
              value={catValue}
              autoComplete="category"
              select
              variant='outlined'>
                <MenuItem value={"Lord"}>Lord</MenuItem>
                <MenuItem value={"Hero"}>Hero</MenuItem>
                <MenuItem value={"meleeInfantry"}>Melee Infantry</MenuItem>
                <MenuItem value={"missileInfantry"}>Missile Infantry</MenuItem>
                <MenuItem value={"cavalry"}>Cavalry</MenuItem>
                <MenuItem value={"artillery"}>Artillery</MenuItem>
                <MenuItem value={"monster"}>Monster</MenuItem>
                </TextField>
            <TextField
              margin="normal"
              onChange= {(e) => setRace(e.target.value)}
              fullWidth
              id="race"
              label="Race"
              name="race"
              autoComplete="race"
              value={raceValue}
              variant='outlined'
            />
            <TextField
              margin="normal"
              onChange= {(e) => setFaction(e.target.value)}
              fullWidth
              id="faction"
              label="Faction"
              name="faction"
              autoComplete="race"
              value={factionValue}
              variant='outlined'
            />
            <TextField
              margin="normal"
              onChange= {(e) => setType(e.target.value)}
              fullWidth
              id="unitType"
              label="Unit Type"
              name="unitType"
              autoComplete="unitType"
              value={typeValue}
              variant='outlined'
            />
            <TextField
              margin="normal"
              onChange= {(e) => setHelth(e.target.value)}
              fullWidth
              id="health"
              label="Health"
              name="health"
              autoComplete={1}
              value={healthValue}
              variant='outlined'
              type='number'
            />
            <TextField
              margin="normal"
              onChange= {(e) => setLeadership(e.target.value)}
              fullWidth
              id="leadership"
              label="Leadership"
              name="leadership"
              autoComplete={1}
              value={leadershipValue}
              variant='outlined'
              type='number'
            />
            <TextField
              margin="normal"
              onChange= {(e) => setSpeed(e.target.value)}
              fullWidth
              id="speed"
              label="Speed"
              name="speed"
              autoComplete={1}
              variant='outlined'
              value={speedValue}
              type='number'
            />
            <TextField
              margin="normal"
              onChange= {(e) => setMAttack(e.target.value)}
              fullWidth
              id="meleeAttack"
              label="Melee Attack"
              name="meleeAttack"
              autoComplete={1}
              value={mAttackValue}
              variant='outlined'
              type='number'
            />
            <TextField
              margin="normal"
              onChange= {(e) => setMDefense(e.target.value)}
              fullWidth
              id="meleeDefense"
              label="Melee Defense"
              name="meleeDefense"
              autoComplete={1}
              value = {mDefenseValue}
              variant='outlined'
              type='number'
            />
            <FormControlLabel name="isUnique" control={
                <Checkbox checked={isLeg} onChange={handleLegCheckbox}
                    
            />} label={"Is Unique"} />
            <Button
              theme = {
                theme.godTheme === 'khorne' ? khorne_theme :
                theme.godTheme === 'tzeench' ? tzeench_theme :
                theme.godTheme === 'slaanesh' ? slaanesh_theme :
                theme.godTheme === 'nurgle' ? nurgle_theme :
                theme.godTheme === 'undivided' ? undivided_theme :
                order_theme}
              color= "God"
              type="submit"
              fullWidth
              disabled={makingReq}
              variant="contained"
              sx={{ mt: 3, mb: 2, 
                fontStyle:{color: "#FFFFFF"}
              }}
            >
              Create Unit
            </Button>
            {gotErr && (
                <Typography theme = {theme} color="error" sx={{fontWeight:'600'}}>
                    {errMsg}
                </Typography>
            )}
        </Box>
        <Footer />
        </Container>
        
      {/* </Box> */}
      </Box>
    </ThemeProvider>
    )
}