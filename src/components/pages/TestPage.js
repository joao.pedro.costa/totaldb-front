'use client'

import * as React from 'react';
import PropTypes from 'prop-types';

import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import { Typography, alpha } from '@mui/material';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import AutoAwesomeRoundedIcon from '@mui/icons-material/AutoAwesomeRounded';
import AppAppBar from '../AppAppBar';
import Hero from '../Hero';

import getLPTheme from '../getLPTheme';


const toggleColorMode = () => {
  setMode((prev) => (prev === 'dark' ? 'light' : 'dark'));
};

const toggleCustomTheme = () => {
  setShowCustomTheme((prev) => !prev);
};

export default function TestPage() {
    
  const [mode, setMode] = React.useState('light');
  const [showCustomTheme, setShowCustomTheme] = React.useState(true);
  const LPtheme = createTheme(getLPTheme(mode));
  const defaultTheme = createTheme({ palette: { mode } });
    return (
        <ThemeProvider theme={ defaultTheme}>
      <CssBaseline />
      <AppAppBar mode={mode} toggleColorMode={toggleColorMode} />
      <Box
      id="test-page"
      sx={(theme) => ({
        width: '100%',
        backgroundImage:
          theme.palette.mode === 'light'
            ? `linear-gradient(180deg, #75192F, ${alpha('#FFF700', 0.7)} 115%)`
            : `linear-gradient(#02294F, ${alpha('#75192F', 0.0)})`,
        backgroundSize: '100% 100%',
        backgroundRepeat: 'no-repeat',
      })}
    >
         <Container
        sx={{
          height: '100%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          pt: { xs: 15, sm: 15},pb: { xs: '100%', sm: '50%' },
        }}
      >
        <Typography 
        textAlign={'center'}
        color={'text.primary'} 
        borderColor={'text.primary'}
        border="medium"
        variant= 'h2'>
           Hello Teste!
           </Typography>
        <div> Teste 2</div>
        </Container>
      </Box>

    </ThemeProvider>
    )
}