'use client'

import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider, useTheme, createTheme } from '@mui/material/styles';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import backgroundColor from '../BackgroundColor';
import { AccountContext } from '../Contexts';
import { mainUrl } from '../../app/ClientComponent';
import { useNavigate} from 'react-router-dom';
import { useCookies } from 'react-cookie';
import { nurgle_theme, khorne_theme, tzeench_theme, slaanesh_theme, order_theme, undivided_theme } from '../Themes'



export default function LoginPage() {
    const theme = useTheme();
    const [cookies, setCookies] = useCookies(["all"],{path:"/"});
    const [gotErr, setErr] = React.useState(false);
    const [gotOk, setOk] = React.useState(false);
    const loginInfo = React.useContext(AccountContext);
    const navigate = useNavigate();

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const creds = {
            email: data.get('email'),
            password: data.get('password'),
          };
        fetch(`http://${mainUrl}/login/${creds.email}/${creds.password}`, {method: 'GET'})
            .then(res => {
                if (res.status == 200) {
                    console.log('settingCookie')
                    setCookies("user", creds.email,{path:"/"})
                    setCookies("logged", true,{path:"/"})
                    
                    setOk(true)
                    navigate("/")
                    navigate(0)
                }
                else {
                    console.log(res.status)
                    setErr(true)
                }
            })
            .catch(e => setErr(true))
      };

    return (
        <ThemeProvider theme={theme}>
            <Box height= {1440} sx = {{
            flexGrow: 1,
            display: 'flex',
            margin: 0,
            width: '100%',
            backgroundImage: backgroundColor(theme.godTheme),
            backgroundSize: '100% 100%',
            padding: 0
        }}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: theme.palette.primary.godColor }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5" color={theme.palette.primary.godColor}>
            Sign in
          </Typography>
          {!gotOk && (
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="User Name"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              theme = {
                theme.godTheme === 'khorne' ? khorne_theme :
                theme.godTheme === 'tzeench' ? tzeench_theme :
                theme.godTheme === 'slaanesh' ? slaanesh_theme :
                theme.godTheme === 'nurgle' ? nurgle_theme :
                theme.godTheme === 'undivided' ? undivided_theme :
                order_theme}
              color= "God"
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2, 
                fontStyle:{color: "#FFFFFF"}
              }}
            >
              Sign In
            </Button>
            {(gotErr &&!gotOk) && (
                <Box>
                    <Typography theme={theme} color="error">
                         Could not find username or password</Typography>
                </Box>
            )} 
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2" color={theme.palette.text.secondary}>
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="/signup" variant="body2" color={theme.palette.text.secondary}>
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
          )}
          {gotOk && (
            <Typography
              theme = {
                theme.godTheme === 'khorne' ? khorne_theme :
                theme.godTheme === 'tzeench' ? tzeench_theme :
                theme.godTheme === 'slaanesh' ? slaanesh_theme :
                theme.godTheme === 'nurgle' ? nurgle_theme :
                theme.godTheme === 'undivided' ? undivided_theme :
                order_theme}
              color= "God"
              variant="contained"
              sx={{ mt: 3, mb: 2, 
                fontStyle:{color: "#FFFFFF"}
              }}
            >
                Logged in! 
            </Typography>
          )}
        </Box>
      </Container>
      </Box>
    </ThemeProvider>
      );

}