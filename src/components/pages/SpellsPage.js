'use client'

import * as React from 'react';
import Footer from '../Footer';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import { Checkbox, FormControl, Grid, FormLabel, Typography, alpha, FormGroup, FormControlLabel, Paper } from '@mui/material';
import { styled, ThemeProvider, useTheme } from '@mui/material/styles';
import AppAppBar from '../AppAppBar';
import backgroundColor from '../BackgroundColor';
import SpellsDataCollection from '../SpellsCollection'

export default function FactionsPage({faction}) {
  const theme = useTheme();
  const [state, setState] = React.useState({
    showDark: false,
    showDeath: false,
    showFire: false,
    showIce: true,
    showLife: false
})

  
const handleChange = (event) => {
    setState({
        ...state,
        [event.target.name]: event.target.checked,
    })
};

const {showDark, showDeath, showFire, showIce, showLife} = state;

  const ToggleBackgroundColor = (newTheme) => {theme.setGodTheme(newTheme);}
  return (
     <ThemeProvider theme = {theme}>
      <CssBaseline />
      {/* <Box height={1440} zIndex={0}> */}
      <AppAppBar toggleColor={ToggleBackgroundColor} />
      
      <Box
      id="factions-page"
      height={1440}
      sx={(theme) => ({
        width: '100%',
        backgroundImage: backgroundColor(theme.godTheme),
        backgroundSize: '100% 100%',
        backgroundRepeat: 'no-repeat',
      })}
    >
         <Container maxWidth={false}
          id = 'container-1'
        sx={{
          height: '100%',
          width: '100%',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          lg: 2400,
          pt: { xs: 5, sm: 10},pb: { xs: 0, sm: 0 },
        }}
      >
        <Container maxWidth={false}
        sx={{
          height: '100%',
          width: '100%',
          flexDirection: 'column',
          alignItems: 'center',
          pt: { xs: 0, sm: 5},pb: { xs: 0, sm: 0},
        }}
      >
        <Grid container spacing={4} alignItems={'top'}>
            <Grid item xs = {'auto'} md={'10'} lg={'auto'} alignContent={'left'}>
          {/* <Drawer variant='permanent' > */}
          <Paper sx ={(theme) =>({
            backgroundColor: 'rgba(255,255,255,0.4)',
            backdropFilter: 'blur(24px)',
            drawerWidth: 240,
            width:350,
            border: '2px solid',
            borderColor: 'divider',
            borderRadius: '0px',
            alignSelf: 'left',
            sm: 240 })}>  
          
            
          <FormControl sx={{m:3}}
            component={'fieldset'}
            variant='standard'
            >
          <Typography theme={theme} variant='h5' color='primary.godColor'> Select Lores of Magic to show</Typography>
          <FormGroup>
            <FormControlLabel
              control={
            <Checkbox checked = {showDark} onChange = {handleChange} name = "showDark"/>
              }
                label= {
                    <React.Fragment>
                        <Box alignItems={'center'}>
                        <img src='/lores/loreOfDark.png'/>
                        Lore of Dark Magic
                        </Box>
                    </React.Fragment>
                }

              />
            <FormControlLabel
              control={
            <Checkbox checked = {showDeath} onChange = {handleChange} name = "showDeath"/>
              }
                label= {
                    <React.Fragment>
                        <Box alignItems={'center'}>
                        <img src='/lores/loreOfDeath.png'/>
                        Lore of Death
                        </Box>
                    </React.Fragment>
                }              
              />
            <FormControlLabel
              control={
            <Checkbox checked = {showFire} onChange = {handleChange} name = "showFire"/>
              }
                label= {
                    <React.Fragment>
                        <Box alignItems={'center'}>
                        <img src='/lores/loreOfFire.png'/>
                        Lore of Fire
                        </Box>
                    </React.Fragment>
                }                
              />
            <FormControlLabel
              control={
            <Checkbox checked = {showIce} onChange = {handleChange} name = "showIce"/>
              }
                label= {
                    <React.Fragment>
                        <Box alignItems={'center'}>
                        <img src='/lores/loreOfIce.png'/>
                        Lore of Ice
                        </Box>
                    </React.Fragment>
                }                
              />
            <FormControlLabel
              control={
            <Checkbox checked = {showLife} onChange = {handleChange} name = "showLife"/>
              }
                label= {
                    <React.Fragment>
                        <Box alignItems={'center'}>
                        <img src='/lores/loreOfLife.png'/>
                        Lore of Life
                        </Box>
                    </React.Fragment>
                }                
              />
 
            </FormGroup>
        </FormControl>
        </Paper>
        {/* </Drawer> */}
            </Grid>

        <Grid item xs = {'auto'} md = {'auto'} lg = {8}>
        <Paper sx ={(theme) =>({
            backgroundColor: 'rgba(255,255,255,0.4)',
            backdropFilter: 'blur(24px)',
            border: '2px solid',
            borderColor: 'divider',
            borderRadius: '0px',
            alignSelf: 'center',
            sm: 240 })}>  

            <SpellsDataCollection state={state}/>
        </Paper>

        </Grid>

        </Grid>



      </Container>
        
        <Footer />
        </Container>
        
      {/* </Box> */}
      </Box>
    </ThemeProvider>
    )
}