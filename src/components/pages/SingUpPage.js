'use client'

import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider, useTheme, createTheme } from '@mui/material/styles';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import backgroundColor from '../BackgroundColor';
import { AccountContext } from '../Contexts';
import { mainUrl } from '../../app/ClientComponent';
import { useNavigate } from 'react-router-dom';
import { useCookies } from 'react-cookie';


const nurgle_theme = createTheme({
    palette: {
      God: {
        light:  "#005221",
        main:  "#005221",
        dark:  "#005221",
        constastText:  "#005221",
      },
    }
  })
  
  const khorne_theme = createTheme({
    palette: {
      God: {
        light:  "#6A0002",
        main:  "#6A0002",
        dark:  "#6A0002",
        constastText:  "#6A0002",
      },
    }
  })
  
  
  const tzeench_theme = createTheme({
    palette: {
      God: {
        light:  "#00506F",
        main:  "#00506F",
        dark:  "#00506F",
        constastText:  "#00506F",
      },
    }
  })
  
  const slaanesh_theme = createTheme({
    palette: {
      God: {
        light:  "#451636",
        main:  "#451636",
        dark:  "#451636",
        constastText:  "#451636",
      },
    }
  })
  
  const undivided_theme = createTheme({
    palette: {
      God: {
        light:  "#393939",
        main:  "#393939",
        dark:  "#393939",
        constastText:  "#393939",
      },
    }
  })
  
  
  const order_theme = createTheme({
    palette: {
      God: {
        light:  "#393121",
        main:  "#393121",
        dark:  "#393121",
        constastText:  "#393121",
      },
    }
  })
  



export default function Page() {
    const theme = useTheme();
    const [gotErr, setErr] = React.useState(false);
    const [gotOk, setOk] = React.useState(false);
    const loginInfo = React.useContext(AccountContext);
    const navigate = useNavigate();

    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const creds = {
            email: data.get('email'),
            password: data.get('password'),
          };
        fetch(`http://${mainUrl}/createUser/${creds.email}/${creds.password}`, {method: 'GET'})
            .then(res => {
                if (res.status == 200) {
                    setOk(true)
                }
                else {
                    console.log(res.status)
                    setErr(true)
                }
            })
            .catch(e => setErr(true))
      };


    return (
        <ThemeProvider theme={theme}>
            <Box height= {1440} sx = {{
            flexGrow: 1,
            display: 'flex',
            margin: 0,
            width: '100%',
            backgroundImage: backgroundColor(theme.godTheme),
            backgroundSize: '100% 100%',
            padding: 0
        }}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: theme.palette.primary.godColor }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5" color={theme.palette.primary.godColor}>
            Sign up
          </Typography>
          {!gotOk && (
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="User Name"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              theme = {
                theme.godTheme === 'khorne' ? khorne_theme :
                theme.godTheme === 'tzeench' ? tzeench_theme :
                theme.godTheme === 'slaanesh' ? slaanesh_theme :
                theme.godTheme === 'nurgle' ? nurgle_theme :
                theme.godTheme === 'undivided' ? undivided_theme :
                order_theme}
              color= "God"
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2, 
                fontStyle:{color: "#FFFFFF"}
              }}
            >
              Sign Up!
            </Button>
            {(gotErr &&!gotOk) && (
                <div>
                    <Typography theme={theme} color="error">
                         Could not create account. Please try again later.</Typography>
                </div>
            )} 
            <Grid container>
              <Grid item xs>
              </Grid>
              <Grid item>
                <Link href="/login" variant="body2" color={theme.palette.text.secondary}>
                  {"Already have an account? Sign in!"}
                </Link>
              </Grid>
            </Grid>
          </Box>
          )}
          {gotOk && (
            <Box>
            <Typography
              theme = {
                theme.godTheme === 'khorne' ? khorne_theme :
                theme.godTheme === 'tzeench' ? tzeench_theme :
                theme.godTheme === 'slaanesh' ? slaanesh_theme :
                theme.godTheme === 'nurgle' ? nurgle_theme :
                theme.godTheme === 'undivided' ? undivided_theme :
                order_theme}
              color= "God"
              variant="contained"
              sx={{ mt: 3, mb: 2, 
                fontStyle:{color: "#FFFFFF"}
              }}
            >
                Account created!

            </Typography>
            <Grid container spacing={2}> 
                <Grid item>
                <Link href={"/"} passHref style={{textDecoration: 'none'}}>
                <Button theme={
                    theme.godTheme === 'khorne' ? khorne_theme :
                    theme.godTheme === 'tzeench' ? tzeench_theme :
                    theme.godTheme === 'slaanesh' ? slaanesh_theme :
                    theme.godTheme === 'nurgle' ? nurgle_theme :
                    theme.godTheme === 'undivided' ? undivided_theme :
                    order_theme}
                color="God"
                id='signup'
                variant="contained"
                size="small"
                target="_blank"
                sx={{fontStyle:{color:"#FFFFFF"}}}
                >
                Back to Home
                </Button>
                </Link>
                </Grid>
                <Grid item>
                <Link href={"/login"} passHref style={{textDecoration: 'none'}}>
                    <Button theme={
                        theme.godTheme === 'khorne' ? khorne_theme :
                        theme.godTheme === 'tzeench' ? tzeench_theme :
                        theme.godTheme === 'slaanesh' ? slaanesh_theme :
                        theme.godTheme === 'nurgle' ? nurgle_theme :
                        theme.godTheme === 'undivided' ? undivided_theme :
                        order_theme}
                    color="God"
                    id='signup'
                    variant="contained"
                    size="small"
                    target="_blank"
                    sx={{fontStyle:{color:"#FFFFFF"}}}
                    >
                    Sign in
                    </Button>
                    </Link>
                    </Grid>
            </Grid>
            </Box>
          )}
        </Box>
      </Container>
      </Box>
    </ThemeProvider>
      );

}