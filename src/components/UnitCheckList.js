'use client'
import * as React from 'react'
import { Checkbox } from "@mui/icons-material";
import { List, Box } from '@mui/material';


export default function UnitCheckList(state, setState) {
    
    const handleChange = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.checked,
        })
    };

    const {showLord, showHero, showInfantry, showMissile, showCavalry,
        showArtillery, showMonster} = state;

    return (
        <React.Fragment>
        <List>
            <Checkbox checked = {showLord} onChange = {handleChange} name = "lord">
                label= "Lords"                
            </Checkbox>
            <Checkbox checked = {showHero} onChange = {handleChange} name = "hero">
                label= "Heroes"                
            </Checkbox>
            <Checkbox checked = {showInfantry} onChange = {handleChange} name = "infantry">
                label= "Melee Infantry"                
            </Checkbox>

        </List>
        </React.Fragment>
    )
}