'use Client'

import Link from '@mui/material/Link'
import * as React from 'react'
import { alpha, useTheme } from '@mui/material/styles'
import IconButton from '@mui/material/IconButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import Tooltip from '@mui/material/Tooltip'
import MenuItem from '@mui/material/MenuItem'
import Menu from '@mui/material/Menu'
import Logout from '@mui/icons-material/Logout'
import Avatar from '@mui/material/Avatar'
import Box from '@mui/material/Box'
import Divider from '@mui/material/Divider'
import { useCookies } from 'react-cookie'
import { useNavigate } from 'react-router-dom'

export default function AccountMenu() {
    const [anchorUsr, setAnchorUsr] = React.useState(null)
    const open = Boolean(anchorUsr);
    const theme = useTheme();
    const navigate = useNavigate();
    const handleClick = (event) => {
        setAnchorUsr(event.currentTarget);
    }



    const [cookies, setCookie, removeCookies, updateCookies] = useCookies(["all"],{path:"/"})
    const handleClose = () => {setAnchorUsr(null)}

    const handleLogout = () => {
        setCookie("user", "",{path:"/"});
        setCookie("logged", false, {path:"/"});
        setAnchorUsr(null);
        navigate("/")
        navigate(0)
    }

    return (
    <React.Fragment>
        <Box sx={{display:'flex', alignItems: 'center', textAlign: 'center'}}>
            <Tooltip title={"Accont Settings"}>
                <IconButton
                    id='avatar-button'
                    onClick={handleClick}
                    
                    size="small"
                    sx={{ml:2}}
                    aria-controls={open? 'account-menu': undefined}
                    aria-haspopup="true"
                    aria-expanded={open? 'true': undefined}
                >
                    
                    <Avatar sx={{width: 32, height: 32, bgcolor: theme.palette.primary.godColor}}>
                        {cookies.user === ""? "A" : Array.from(cookies.user)[0].toUpperCase()}
                    </Avatar>
                </IconButton>
            </Tooltip>
        </Box>
        <Menu
            anchorEl={anchorUsr}
            id='account-menu'
            open={open}
            onClose={handleClose}
            onClick={handleClose}
            MenuListProps={{
                "aria-labelledby":'avatar-button'
            }}
            color={alpha(theme.palette.primary.godColor, 0.35)}
            slotProps={{
                paper: {
                    elevation: 0,
                    sx: {
                        overflow: 'visible',
                        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32)',
                        mt: 10,
                        bgcolor: alpha(theme.palette.primary.godColor, 0.35),
                        '& .MuiMenu-list': {
                            padding:'4px',
                            bgcolor: alpha(theme.palette.primary.godColor, 0.35)

                        },
                        '& .MuiMenu-paper': {
                            bgcolor: alpha(theme.palette.primary.godColor, 0.35)
,                        },
                        '& .MuiAvatar-root': {
                            width:32,
                            height:32,
                            ml: -0.5,
                            mr:1
                        },
                        '&:before': {
                            content: '"',
                            display: 'block',
                            // position:'absolute',
                            top:10,
                            right:14,
                            width:10,
                            height:10,
                            bgcolor: 'background.paper',
                            transform: 'translateY(50%) rotate(45deg)',
                            zIndex:0
                        },
                        '&:active': {
                            backgroundColor:alpha(theme.palette.primary.godColor, 0.35)
                        }
                    }
                }
            }} 
            transformOrigin={{horizontal:"right", vertical:"bottom"}}
            anchorOrigin={{horizontal:"right", vertical:"bottom"}}
        >
            <MenuItem onClick={handleClose}> 
               <Link href={"/user/createUnits"}> Create Unit </Link>
            </MenuItem>
            <MenuItem onClick={handleClose}>
            <Link href={"/user/myUnits"}> My Units </Link>
            </MenuItem>
            <Divider />
            <MenuItem onClick={handleLogout}>
                <ListItemIcon>
                    <Logout fontSize='small'/>
                </ListItemIcon>
                Logout
            </MenuItem>
        </Menu>
    </React.Fragment>
    )
}