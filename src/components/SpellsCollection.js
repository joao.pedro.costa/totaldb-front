'use Client'

import * as React from 'react'
import { useTheme } from "@mui/system"
import {Pagination, alpha } from '@mui/material';
import Box from '@mui/material/Box'
import { DataGrid , gridClasses} from '@mui/x-data-grid';
import { useCookies } from 'react-cookie';

const columns = [
    {field: 'name', headerName: 'Name', width:255, headerClassName: 'grid-theme--header'},
    {field: 'abilityType', headerName: 'Ability Type', width: 155, headerClassName: 'grid-theme--header'},
    {field: 'useCount', headerName: 'Uses', width: 100, headerClassName: 'grid-theme--header'},
    {field: 'lore', headerName: 'Lore of Magic', width: 125, headerClassName: 'grid-theme--header'},
    {field: 'duration', headerName: 'Duration', width: 100, headerClassName: 'grid-theme--header'},
    {field: 'cooldown', headerName: 'Cooldown', width: 100, headerClassName: 'grid-theme--header'},
    {field: 'cost', headerName: 'Winds of Magic', width: 125, headerClassName: 'grid-theme--header'},
    {field: 'canOvercast', headerName: 'Can Overcast', width: 125, headerClassName: 'grid-theme--header', type: 'boolean'},
    {field: 'overcastCost', headerName: 'Overcast Cost', width: 125, headerClassName: 'grid-theme--header'},
    {field: 'affected', headerName: 'Affected Units', width: 175, headerClassName: 'grid-theme--header'},
    {field: 'effects', headerName: 'Effects', width: 255, headerClassName: 'grid-theme--header'},
]
    

function formatCategory(category) {
    if (category === "meleeInfantry") return "Melee Infantry";
    if (category === "missileInfantry") return "Missile Infantry";
    else return category
}

function fullLoreName(lore) {
    if (lore === 'loreOfIce') return "Lore of Ice"
    if (lore === 'loreOfDark') return "Lore of Dark Magic"
    if (lore === 'loreOfDeath') return "Lore of Death"
    if (lore === 'loreOfFire') return "Lore of Fire"
    if (lore === 'loreOfLife') return "Lore of Life"
    return "Unknown"
}

function createData(unitData, index, lore) {
    return {
      id: index,
      name: unitData.name,
      abilityType: unitData.ablType,
      useCount: unitData.useCount < 0? '∞' : unitData.useCount,
      duration: unitData.duration,
      cooldown: unitData.cooldown,
      cost: unitData.windsCost,
      canOvercast: unitData.overcastCost > 0,
      overcastCost: unitData.overcastCost > 0? unitData.overcastCost : 0,
      effects: unitData.effects.toString(),
      affected: unitData.affectedUnits,
      lore: fullLoreName(lore),
      }
  }


export default function SpellsDataCollection({state}) {
    const theme = useTheme();
    const [rows, setRows] = React.useState([])
    const requests = {
        loreOfDark: state.showDark,
        loreOfDeath: state.showDeath,
        loreOfFire: state.showFire,
        loreOfIce: state.showIce,
        loreOfLife: state.showLife,
    }

    const [cookies, setCookies, removeCookies, updateCookies] = useCookies(["all"],{path:"/"})
    
    const getSpells = () => {
        var new_units = [];
        setRows([])
        updateCookies();
        console.log(state)
        for (let [lore, check] of Object.entries(requests)) {
            
            if (check) {
                console.log(`requesting ${lore}: ${check}`)
                fetch(`http://192.168.0.172:8080/db/getAllSpells/${lore}`, {
                    method: "GET",
                  })
                  .then(res => {
                    return res.json()
                  })
                  .then(data => {
                    for (let unit of data) {
                        new_units = new_units.concat(createData(unit, new_units.length, lore))
                        setRows(new_units)
                        
                    }
                  })
                  .catch(e => {console.log(e)})
                }
        }
    }

    React.useEffect( () => getSpells(), [
        state.showDeath,
        state.showDark,
        state.showFire,
        state.showIce,
        state.showLife
    ])

    return (
        <div style ={{height:'auto', width:'100%'}}>
            <Box 
                sx={{
                    '& .grid-theme--header': {
                        backgroundColor: alpha(theme.palette.primary.godColor, 0.55),
                        fontWeight: '600',
                        fontSize: 16
                    },
                    '& .grid-theme--even-row': {
                        backgroundColor: alpha(theme.palette.text.primary, 0.2),
                    },
                    '& .grid-theme--odd-row': {
                        backgroundColor: alpha(theme.palette.primary.godColor, 0.5)
                    }

                }}
            >
            <DataGrid
                rows={rows}
                columns={columns}
                autoHeight = {true}
                sx={{
                    fontSize: 16,
                }}
                initialState={{
                    pagination: {
                        paginationModel: {page: 0, pageSize: 30}
                    },
                }}
                pageSizeOptions={[5,10,20,30]}
                getRowClassName={(params) => params.indexRelativeToCurrentPage % 2 === 0? 
                    'grid-theme--even-row' :
                    'grid-theme--odd-row' 
                }
                />
                </Box>
        </div>
    )

    
}