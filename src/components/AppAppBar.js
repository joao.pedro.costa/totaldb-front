import * as React from 'react';
import PropTypes from 'prop-types';

import {
  createTheme,
  ThemeProvider,
  alpha,
} from '@mui/material/styles'

import { useTheme } from '@mui/material/styles';
import Link from 'next/link'
// import {Link as ReactLink} from 'react-router-dom';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import MenuItem from '@mui/material/MenuItem';
import Drawer from '@mui/material/Drawer';
import MenuIcon from '@mui/icons-material/Menu';
import {AccountContext} from './Contexts';
import AccountMenu from './AccountMenu';
import { Cookies, useCookies } from 'react-cookie';

const logoStyle = {
  width: '140px',
  height: 'auto',
  cursor: 'pointer',
};

const nurgle_theme = createTheme({
  palette: {
    God: {
      light:  "#005221",
      main:  "#005221",
      dark:  "#005221",
      constastText:  "#005221",
    },
  }
})

const khorne_theme = createTheme({
  palette: {
    God: {
      light:  "#6A0002",
      main:  "#6A0002",
      dark:  "#6A0002",
      constastText:  "#6A0002",
    },
  }
})


const tzeench_theme = createTheme({
  palette: {
    God: {
      light:  "#00506F",
      main:  "#00506F",
      dark:  "#00506F",
      constastText:  "#00506F",
    },
  }
})

const slaanesh_theme = createTheme({
  palette: {
    God: {
      light:  "#451636",
      main:  "#451636",
      dark:  "#451636",
      constastText:  "#451636",
    },
  }
})

const undivided_theme = createTheme({
  palette: {
    God: {
      light:  "#393939",
      main:  "#393939",
      dark:  "#393939",
      constastText:  "#393939",
    },
  }
})


const order_theme = createTheme({
  palette: {
    God: {
      light:  "#393121",
      main:  "#393121",
      dark:  "#393121",
      constastText:  "#393121",
    },
  }
})


function AppAppBar({toggleColor }) {
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const toggleDrawer = (newOpen) => () => {
    setOpen(newOpen);
  };

  
  const setOrder = () => {toggleColor('order')}
  const setUndivided = () => {toggleColor('undivided')}
  const setNurgle = () => {toggleColor('nurgle')}
  const setKhorne = () => {toggleColor('khorne')}
  const setTzeench = () => {toggleColor('tzeench')}
  const setSlaanesh = () => {toggleColor('slaanesh')}

  const [cookies] = useCookies(["all"],{path:"/"})
  const logged = cookies.logged

  return (
    <Box>
      <AppBar
        position="fixed"
        id = "app-bar"
        sx={{
          boxShadow: 0,
          bgcolor: 'transparent',
          backgroundImage: 'none',
          mt: 2,
        }}
      >
        <Container maxWidth="lg">
          <Toolbar
            variant="regular"
            sx={(theme) => ({
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-between',
              flexShrink: 0,
              borderRadius: '0px',
              bgcolor:'rgba(255, 255, 255, 0.4)',
              backdropFilter: 'blur(24px)',
              maxHeight: 120,
              border: '2px solid',
              borderColor: 'divider',
              boxShadow:`0 0 1px rgba(85, 166, 246, 0.1), 1px 1.5px 2px -1px rgba(85, 166, 246, 0.15), 4px 4px 12px -2.5px rgba(85, 166, 246, 0.15)`,
            })}
          >
            <Box
              sx={{
                flexGrow: 1,
                display: 'flex',
                alignItems: 'center',
                ml: '-18px',
                px: 0,
              }}
            >
              <Link href="/" passHref style={{textDecoration: 'none'}}>
              <img
                src={
                  '/logo.png'
                }
                style={logoStyle}
                alt="logo of TotalDB"
              />
              </Link>
              <Box sx={{ display: { xs: 'none', md: 'flex' } }}>

                <Link href = "/spells" passHref style={{textDecoration: 'none'}}>
                <MenuItem
                  sx={{ py: '6px', px: '12px' }}
                >
                  <Typography variant="h6" color="text.primary">
                    Spells
                  </Typography>
                </MenuItem>
                </Link>
              </Box>
            </Box>
            <Box
              sx={{
                display: { xs: 'none', md: 'flex' },
                gap: 0.5,
                alignItems: 'center',
              }}
            >
              <Button 
                variant = "text"
                onClick = {setOrder}
                aria-label='change to order theme'
                sx = {{minWidth: "12px", height: "48px"}}>
                  <img
                    src= '/gods/order.png'
                    
                    height={'48px'}
                    width={'48px'}
                    alt='order_symbol'></img>

                </Button>

              
                <Button 
                variant = "text"
                onClick = {setUndivided}
                aria-label='change to order theme'
                sx = {{minWidth: "12px", height: "48px"}}>
                  <img
                    src= '/gods/undivided.png'
                    
                    height={'48px'}
                    width={'48px'}
                    alt='undivided_symbol'></img>

                </Button>


                <Button 
                variant = "text"
                onClick = {setNurgle}
                aria-label='change to order theme'
                sx = {{minWidth: "12px", height: "48px"}}>
                  <img
                    src= '/gods/nurgle.png'
                    
                    height={'48px'}
                    width={'48px'}
                    alt='nurgle_symbol'></img>

                </Button>

                <Button 
                variant = "text"
                onClick = {setKhorne}
                aria-label='change to order theme'
                sx = {{minWidth: "12px", height: "48px"}}>
                  <img
                    src= '/gods/khorne.png'
                    
                    height={'48px'}
                    width={'48px'}
                    alt='khorne_symbol'></img>

                </Button>

                <Button 
                variant = "text"
                onClick = {setTzeench}
                aria-label='change to order theme'
                sx = {{minWidth: "12px", height: "48px"}}>
                  <img
                    src= '/gods/tzeench.png'
                    
                    height={'48px'}
                    width={'48px'}
                    alt='tzeench_symbol'></img>

                </Button>

                <Button 
                variant = "text"
                onClick = {setSlaanesh}
                aria-label='change to order theme'
                sx = {{minWidth: "12px", height: "48px"}}>
                  <img
                    src= '/gods/slaanesh.png'
                    
                    height={'48px'}
                    width={'48px'}
                    alt='slaanesh_symbol'></img>

                </Button>
                {!logged && (
                  <Box >
                    <Link href={"/login"} passHref style={{textDecoration: 'none'}}>
                      <Button theme={
                          theme.godTheme === 'khorne' ? khorne_theme :
                          theme.godTheme === 'tzeench' ? tzeench_theme :
                          theme.godTheme === 'slaanesh' ? slaanesh_theme :
                          theme.godTheme === 'nurgle' ? nurgle_theme :
                          theme.godTheme === 'undivided' ? undivided_theme :
                          order_theme}
                        color="God"
                        id='sign'
                        variant="text"
                        size="small"
                        target="_blank"
                      >
                        Sign in
                      </Button>
                      </Link>

                    <Link href={"/signup"} passHref style={{textDecoration: 'none'}}>
                      <Button theme={
                          theme.godTheme === 'khorne' ? khorne_theme :
                          theme.godTheme === 'tzeench' ? tzeench_theme :
                          theme.godTheme === 'slaanesh' ? slaanesh_theme :
                          theme.godTheme === 'nurgle' ? nurgle_theme :
                          theme.godTheme === 'undivided' ? undivided_theme :
                          order_theme}
                        color="God"
                        id='signup'
                        variant="contained"
                        size="small"
                        target="_blank"
                        sx={{fontStyle:{color:"#FFFFFF"}}}
                      >
                        Sign up
                      </Button>
                      </Link>
                </Box>)}
                {logged && (
                  <AccountMenu/>
                )}            



            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    </Box>
  );
}

AppAppBar.propTypes = {
  mode: PropTypes.oneOf(['dark', 'light']).isRequired,
  toggleColorMode: PropTypes.func.isRequired,
};

export default AppAppBar;
