import "./globals.css";
// import AppRouterCacheProvider from '@mui/material-nextjs/v14-appRouter'
// import theme from "./theme"
import ClientComponent from "./ClientComponent"

export const metadata = {
  title: "TotalDB",
  description: "A database for TotalWahammer3 units",
};

export default function RootLayout({ children }) {

  return (
    <>
      <ClientComponent children={children} />
    </>
  )
}
