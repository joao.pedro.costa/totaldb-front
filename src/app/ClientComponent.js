'use client'

import * as React from 'react';
import getLPTheme from '../components/getLPTheme';
import { ThemeProvider , createTheme} from "@mui/material/styles";
const inter = Inter({ subsets: ["latin"] });
import { Inter } from "next/font/google";
import {AccountContext, NavigateContext} from '../components/Contexts'
import { CookiesProvider, useCookies} from 'react-cookie';
import { BrowserRouter } from 'react-router-dom';

export const mainUrl = "localhost:8080"

export default function ClientComponent({children}) {
    const [mode, setMode] = React.useState('light');
        
    const [cookies, setCookies] = useCookies(["all"])
    if (cookies.godTheme === undefined) setCookies("godTheme", "order")
    const [godTheme, setGodTheme] = React.useState(cookies.godTheme);

    var theme = createTheme(getLPTheme(mode, godTheme));

    theme.setGodTheme = (newtheme) => {
        setCookies("godTheme", newtheme,{path:"/"});
        setGodTheme(newtheme);
    }
    theme.godTheme = godTheme
    theme.setMode = setMode
    const [usr, setUsr] = React.useState({usr:"teste",pass:"", logged: false})

    const loginFunc = (user, pass) => {
         setUsr({usr:user, pass:pass, logged: true})
    }
    
    const logoutFunc = () => {
        setUsr({usr: "", pass: "", logged: false})
    }

    const loginContext = {user: usr, loginFunc: loginFunc, logoutFunc: logoutFunc}

    return (
        <html lang="en">
        <body className={inter.className}>
            {/* <AppRouterCacheProvider> */}
            <ThemeProvider theme={theme}>
                <CookiesProvider>
                <AccountContext.Provider value={loginContext}>
                    <BrowserRouter> {children}</BrowserRouter>
                </AccountContext.Provider>
                </CookiesProvider>
            </ThemeProvider>
            {/* </AppRouterCacheProvider> */}
        </body>
        </html>
    )
}