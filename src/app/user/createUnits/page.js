'use client'
import CreateUnitPage from '../../../components/pages/CreateUnitPage'

export default function Page() {
    return (
        <CreateUnitPage />
    )
}