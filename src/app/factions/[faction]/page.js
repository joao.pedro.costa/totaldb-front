import FactionPage from '../../../components/pages/FactionsPage'


export default function FactionsPage({params}) {
    
    return (
        <FactionPage faction = {params.faction} />
    )

}